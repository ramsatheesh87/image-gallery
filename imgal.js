var data; 
var _index;

var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
			if(xhr.readyState===4 && xhr.status===200 ){
				data = JSON.parse(xhr.responseText);
				data = data.concat(data,data);
				image_load();
			}
	}
	xhr.open("GET","https://api.unsplash.com/photos/?client_id=DgrslbgYqOW3lT3XZwl_eaP5IZ7rGk9WAhIjWiC1zog",true);
	xhr.send();	

function image_load(){
	$.each(data,function(i) {
		$('.imgal_container').append('<img src='+data[i].urls.regular+' alt="Image Alt Text" class="imgal-img">');
    });
}

$(document).off('click','.imgal-img');
$(document).on('click','.imgal-img',function(){
	 _index = $(this).index();
	 img_popup(_index)
})
 
function img_popup(_index){
	$('.imgal_container').hide();
	$('.img_overlay').show();
	$('.imgal_modal').find('img').attr("src",data[_index].urls.regular);
	$('.img_logo').find('img').attr("src",data[_index].user.profile_image.small);
	$('.img_text').find('span').eq(0).html(data[_index].user.name);
	$('.img_text').find('a').removeClass('bluecolor');
	
	if(data[_index].user.for_hire){
		$('.img_text').find('a').addClass('bluecolor');
		$('.img_text').find('a').html('Available for hire');
	}else if(data[_index].sponsorship!=null){
		$('.img_text').find('a').html(data[_index].sponsorship.tagline);
	}else{
		$('.img_text').find('a').html('@'+data[_index].user.name);
	}
	
	if(data[_index].user.portfolio_url!=null){
		$('.img_text').find('a').attr("href",data[_index].user.portfolio_url);
	}else{
		$('.img_text').find('a').attr("href",data[_index].user.links.html);
	}
	$('.download_img').attr("href",data[_index].urls.regular); 
	
	if(_index==0){
		$('.prevbtn span').removeClass('enable')
		$('.nextbtn span').addClass('enable')
	}else if(_index == data.length-1){
		$('.nextbtn span').removeClass('enable')
	}else{
		$('.nextbtn span,.prevbtn span').addClass('enable')
	}
}

$(document).off('click','#imgal_modal_close');
$(document).on('click','#imgal_modal_close',function(){
	$('.imgal_container').show();
	$('.img_overlay').hide();
});

$(document).off('click','.nextbtn span.enable');
$(document).on('click','.nextbtn span.enable',function(){
	_index++
	img_popup(_index)
});

$(document).off('click','.prevbtn span.enable');
$(document).on('click','.prevbtn span.enable',function(){
	_index--
	img_popup(_index)
});
